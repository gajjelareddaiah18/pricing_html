
  document.addEventListener("DOMContentLoaded", function() {
    document.querySelector(".switch input[type='checkbox']").addEventListener("change", function() {
      const isChecked = this.checked;
      if (isChecked) {
        // If the switch is checked (monthly), update prices to monthly values
        document.querySelector("#basic-price").textContent = "$19.99";
        document.querySelector("#professional-price").textContent = "$24.99";
        document.querySelector("#master-price").textContent = "$39.99";
       
      } else {
        // If the switch is not checked (annually), update prices to annual values
  
        document.querySelector("#basic-price").textContent = "$199.99";
        document.querySelector("#professional-price").textContent = "$249.99";
        document.querySelector("#master-price").textContent = "$399.99";
      
      }
    });
  });

